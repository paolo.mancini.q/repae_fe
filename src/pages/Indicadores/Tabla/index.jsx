import React, { useEffect } from 'react';
import { mergeRight, propOr } from 'ramda';
import { Indicadores, Instituciones } from '../../../redux/actions';
import { useDispatch, useSelector } from 'react-redux';
import IndicadoresTable from './IndicadoresTable';
import ResourceRender from 'ghost-stories/dist/react/resource';
import { Resource } from 'ghost-stories/dist/unions/Resource';
import { Card, CardContent, Button } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import { Link } from 'react-router-dom';
import { OnQuery, OnError, OnEmpty } from '../../ResourceHelpers';
import ROUTES from '../../../constants/routes';

export default () => {
  const dispatch = useDispatch();
  const indicadores = useSelector((state) => state.indicadores.lista);
  const instituciones = useSelector((state) => state.instituciones.lista);
  useEffect(() => {
    dispatch(Indicadores(indicadores.update()));
    dispatch(Instituciones(instituciones.update()));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const indicadoresFull = Resource.concat([indicadores, instituciones]).map(
    ([indicadores, instituciones]) =>
      indicadores.map((ind) =>
        mergeRight(ind, {
          institucion: propOr(
            '',
            'nombre',
            instituciones.data.find((inst) => inst.id === ind.institucion)
          )
        })
      )
  );
  return (
    <Card className="card-box mb-4">
      <div className="card-header pr-2">
        <div className="card-header--title">
          <h6 className="font-size-lg mt-2 text-dark">Indicadores</h6>
        </div>
        <div className="card-header--actions">
          <Button
            size="small"
            variant="contained"
            color="secondary"
            component={Link}
            to={ROUTES.INDICADORES_AGREGAR}
            startIcon={<AddIcon />}>
            Agregar
          </Button>
        </div>
      </div>
      <CardContent className="p-3">
        <ResourceRender
          resource={indicadoresFull}
          Data={IndicadoresTable}
          Query={OnQuery}
          Empty={OnError}
          Error={OnEmpty}
          props={{ instituciones }}
        />
      </CardContent>
    </Card>
  );
};
