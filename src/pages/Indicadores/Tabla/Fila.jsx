import React, { useCallback } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IconButton, Tooltip } from '@material-ui/core';
import { useDispatch } from 'react-redux';
import { EliminarIndicador } from '../../../redux/actions';
import { Resource } from 'ghost-stories/dist/unions/Resource';
import { FIX_URL } from 'ghost-stories/dist/constants';
import VerMas from './VerMas';
import ROUTES from '../../../constants/routes';
import { Link } from 'react-router-dom';

export default (props) => {
  const { indicador } = props;
  const dispatch = useDispatch();
  const onDeleteIndicador = useCallback(() => {
    dispatch(EliminarIndicador(Resource.Query({ [FIX_URL]: indicador.id })));
  }, [dispatch, indicador.id]);
  return (
    <tr key={indicador.id}>
      <td className="text-left">{indicador.codigo}</td>
      <td className="text-left">{indicador.nombre}</td>
      <td className="text-left">{indicador.institucion}</td>
      <td className="text-left">{indicador.descripcion}</td>
      <td className="text-left">{indicador.creadorNombre}</td>
      <td>
        <Tooltip arrow title="Editar">
          <IconButton
            size="small"
            variant="outlined"
            color="secondary"
            component={Link}
            to={`${ROUTES.INDICADORES_EDITAR.replace(':id', indicador.id)}`}>
            <FontAwesomeIcon icon={['fa', 'edit']} />
          </IconButton>
        </Tooltip>
      </td>

      <td className="text-center">
        <Tooltip arrow title="Eliminar">
          <IconButton
            size="small"
            variant="outlined"
            color="secondary"
            onClick={onDeleteIndicador}>
            <FontAwesomeIcon icon={['fa', 'trash']} />
          </IconButton>
        </Tooltip>
      </td>

      <td className="text-center">
        <VerMas indicador={indicador} />
      </td>
    </tr>
  );
};
