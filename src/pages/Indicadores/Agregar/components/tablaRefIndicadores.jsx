import React from 'react';
import { Card, Grid, CardContent } from '@material-ui/core';
import FormAgregarRefIndicadores from './formAgregarRefIndicadores';
import { useSelector } from 'react-redux';
import ReferenciaCard from './referenciaCard';

export default () => {
  const referencias = useSelector(
    (state) => state.indicadores.formulario.referencias
  );
  return (
    <Grid item xs={12}>
      <Card className="mb-4">
        <div className="card-header">
          <div className="card-header--title font-size-md font-weight-bold py-2">
            Referencias
          </div>
        </div>
        <CardContent>
          {referencias.map((referencia, i) => (
            <ReferenciaCard
              key={i.toString()}
              referencia={referencia}
              indice={i}
            />
          ))}
        </CardContent>
        <FormAgregarRefIndicadores />
      </Card>
    </Grid>
  );
};
