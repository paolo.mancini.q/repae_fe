import React from 'react';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { TextField } from '@material-ui/core';

export default ({ value, handlers }) => {
  return (
    <Autocomplete
      multiple
      disableCloseOnSelect
      noOptionsText="No hay coincidencias"
      options={value.data}
      onChange={handlers.onChange}
      getOptionLabel={(option) => option.ODS_meta_nro}
      renderOption={(option) => (
        <React.Fragment>
          <strong className="mr-3">({option.ODS_meta_nro})</strong>
          <p> {option.ODS_meta_nombre}</p>
        </React.Fragment>
      )}
      renderInput={(params) => (
        <TextField
          {...params}
          label="ODS"
          variant="outlined"
          className="mb-3"
          fullWidth
        />
      )}
    />
  );
};
