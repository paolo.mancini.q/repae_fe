import React, { Fragment, useState, useEffect, useCallback } from 'react';
import { EditarIndicador, Ods } from '../../../../redux/actions';
import { useDispatch, useSelector } from 'react-redux';
import { Dialog, Button, Grid, Container } from '@material-ui/core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Data from './data';
import ResourceRender from 'ghost-stories/dist/react/resource';

export default () => {
  const [modal1, setModal1] = useState(false);
  const toggle1 = () => setModal1(!modal1);
  const dispatch = useDispatch();
  const ods = useSelector((state) => state.ods.ods);
  useEffect(() => {
    dispatch(Ods(ods.update()));
  }, []);
  const formulario = useSelector((state) => state.indicadores.formulario);

  const onChangeODS = useCallback(
    (event, ods) => {
      dispatch(
        EditarIndicador(
          Object.assign({}, formulario, {
            ods: ods.map((x) => x)
          })
        )
      );
    },
    [formulario, dispatch]
  );

  return (
    <Fragment>
      <div className="card-footer p-3 text-right">
        <Button
          onClick={toggle1}
          size="small"
          color="secondary"
          variant="contained">
          <span className="btn-wrapper--label">Agregar</span>
          <span className="btn-wrapper--icon">
            <FontAwesomeIcon icon={['fas', 'plus']} />
          </span>
        </Button>
      </div>
      <Dialog fullScreen open={modal1} onClose={toggle1}>
        <Container>
          <div className="card-header-alt d-flex justify-content-between p-4">
            <h6 className="font-weight-bold font-size-lg mb-1 text-black">
              Agregar ODS
            </h6>
          </div>
          <form>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={2}></Grid>

              <Grid item xs={12} sm={8}>
                <ResourceRender
                  resource={ods}
                  Data={Data}
                  handlers={{ onChange: onChangeODS }}
                />
              </Grid>

              <Grid item xs={12} sm={2}></Grid>
            </Grid>
          </form>
        </Container>
        <div className="card-footer p-3 text-right">
          <Button
            onClick={toggle1}
            size="small"
            color="secondary"
            variant="contained">
            <span className="btn-wrapper--label">Aceptar</span>
          </Button>
        </div>
      </Dialog>
    </Fragment>
  );
};
