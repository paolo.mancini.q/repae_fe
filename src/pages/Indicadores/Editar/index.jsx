import React, { useCallback, useEffect } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { Resource } from 'ghost-stories/dist/unions/Resource';
import { FIX_URL } from 'ghost-stories/dist/constants';

import { useSelector, useDispatch } from 'react-redux';
import {
  ModificarIndicador,
  EditarIndicador,
  Instituciones,
  LimpiarIndicador
} from '../../../redux/actions';

import FormularioIndicadores from '../FormIndicadores';

export default () => {
  const { id } = useParams();

  const history = useHistory();
  const dispatch = useDispatch();
  const formulario = useSelector((state) => state.indicadores.formulario);
  const instituciones = useSelector((state) => state.instituciones.lista);

  const original = useSelector((state) =>
    state.indicadores.lista
      .map((xs) => xs.find((x) => x.id === id))
      .getDataOr(null)
  );

  useEffect(() => {
    dispatch(Instituciones(instituciones.update()));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (original === null) {
      return history.goBack();
    }
    dispatch(EditarIndicador(original));

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const onChageActivo = useCallback(
    (event) => {
      dispatch(
        EditarIndicador({ ...formulario, activo: event.target.checked })
      );
    },
    [formulario, dispatch]
  );

  const onChangeCodigo = useCallback(
    (event) => {
      dispatch(EditarIndicador({ ...formulario, codigo: event.target.value }));
    },
    [formulario, dispatch]
  );

  const onChangeNombre = useCallback(
    (event) => {
      dispatch(EditarIndicador({ ...formulario, nombre: event.target.value }));
    },
    [formulario, dispatch]
  );

  const onChangeUnidadMedida = useCallback(
    (event) => {
      dispatch(
        EditarIndicador({ ...formulario, unidadMedida: event.target.value })
      );
    },
    [formulario, dispatch]
  );

  const onChangeFormulas = useCallback(
    (event) => {
      dispatch(EditarIndicador({ ...formulario, formula: event.target.value }));
    },
    [formulario, dispatch]
  );

  const onChangeFrecuencia = useCallback(
    (event) => {
      dispatch(
        EditarIndicador({ ...formulario, frecuencia: event.target.value })
      );
    },
    [formulario, dispatch]
  );

  const onChangeDescripcion = useCallback(
    (event) => {
      dispatch(
        EditarIndicador({ ...formulario, descripcion: event.target.value })
      );
    },
    [formulario, dispatch]
  );

  const onChangeInstitucion = useCallback(
    (event, institucion) => {
      dispatch(EditarIndicador({ ...formulario, institucion: institucion.id }));
    },
    [formulario, dispatch]
  );

  const onChangeTendencia = useCallback(
    (event, tendencia) => {
      dispatch(EditarIndicador({ ...formulario, tendencias: tendencia }));
    },
    [formulario, dispatch]
  );

  const onCancelar = useCallback(() => {
    dispatch(LimpiarIndicador({}));
    history.goBack();
  }, [dispatch]);

  const onGuardar = useCallback(
    (event) => {
      event.preventDefault();
      dispatch(
        ModificarIndicador(Resource.Query({ [FIX_URL]: id, ...formulario }))
      );
      history.goBack();
    },
    [dispatch, id, formulario, history]
  );

  const onAgregarInsitucion = useCallback(() => {
    history.goBack();
  }, [history]);
  return (
    <FormularioIndicadores
      {...{
        formulario,
        instituciones,
        onChageActivo,
        onChangeCodigo,
        onChangeNombre,
        onChangeUnidadMedida,
        onChangeFormulas,
        onChangeFrecuencia,
        onChangeDescripcion,
        onChangeInstitucion,
        onChangeTendencia,
        onCancelar,
        onGuardar,
        onAgregarInsitucion
      }}
    />
  );
};
