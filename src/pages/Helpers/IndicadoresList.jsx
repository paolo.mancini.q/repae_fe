import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
export default ({ titulo, valor, parametros }) => {
  return (
    <>
      <div className="d-flex justify-content-between ">
        <span className="font-weight-bold">{titulo}</span>
      </div>
      <div className="d-flex justify-content-between ">
        <TableContainer component={Paper}>
          <Table size="small" aria-label="a dense table">
            <TableHead>
              <TableRow>
                <TableCell align="right">Indicador</TableCell>
                <TableCell align="right">Geografía</TableCell>
                <TableCell align="right">Sexo</TableCell>
                <TableCell align="right">Rango Etario</TableCell>
                <TableCell align="right">Valor Medido</TableCell>
                <TableCell align="right">Valor de Referencia</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {valor.map((i) => {
                const geografia = parametros.region.find(
                  (x) => x.id === i.geografia
                );
                const indicador = parametros.indicadores.find(
                  (x) => x.id === i.indicador
                );
                const edad = parametros.rango_etario.find(
                  (x) => x.id === i.edad
                );
                const sexo = parametros.sexo.find((x) => x.id === i.sexo);
                if (!indicador) {
                  return null;
                }
                return (
                  <TableRow key={Math.random().toString()}>
                    <TableCell component="th" scope="row">
                      {indicador.nombre}
                    </TableCell>
                    <TableCell align="right">{geografia.nombre}</TableCell>
                    <TableCell align="right">{sexo.nombre}</TableCell>
                    <TableCell align="right">{edad.rango}</TableCell>
                    <TableCell align="right">{i.v_medido}</TableCell>
                    <TableCell align="right">{i.v_referencia}</TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </TableContainer>
      </div>
    </>
  );
};
// v_referencia: '456',
// v_medido: '789',
// tableData: { id: 0 },
// otrasDimensiones: '123'
