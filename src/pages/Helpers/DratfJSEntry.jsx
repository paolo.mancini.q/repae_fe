import React from 'react';
import draftToHtml from 'draftjs-to-html';

export default ({ titulo, valor }) => {
  const markup = { __html: draftToHtml(valor) };
  return (
    <>
      <div className="d-flex justify-content-between ">
        <span className="font-weight-bold">{titulo}</span>
      </div>
      <div className="d-flex justify-content-between ">
        <p className="text-black-50" dangerouslySetInnerHTML={markup}></p>
      </div>
    </>
  );
};
