import React from 'react';

export default ({ titulo, valor }) => {
  return (
    <>
      <div className="d-flex justify-content-between ">
        <span className="font-weight-bold">{titulo}</span>
      </div>
      <div className="d-flex justify-content-between ">
        <ul>
          {valor.map((ods) => (
            <li key={ods.id}>
              <span className="font-weight-bold">{ods.ODS_meta_nro}: </span>
              <span>{ods.ODS_meta_nombre}</span>
            </li>
          ))}
        </ul>
      </div>
    </>
  );
};
