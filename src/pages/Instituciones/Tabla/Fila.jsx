import React, { useCallback } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IconButton, Tooltip } from '@material-ui/core';
import { useDispatch } from 'react-redux';
import { FIX_URL } from 'ghost-stories/dist/constants';
import { EliminarInstitucion } from '../../../redux/actions';
import { Resource } from 'ghost-stories/dist/unions/Resource';
import VerMas from './VerMas';
import { Link } from 'react-router-dom';
import ROUTES from '../../../constants/routes';

export default (props) => {
  const { institucion } = props;
  const dispatch = useDispatch();
  const onDeleteInstitucion = useCallback(() => {
    dispatch(
      EliminarInstitucion(Resource.Query({ [FIX_URL]: institucion.id }))
    );
  }, [dispatch, institucion]);

  return (
    <tr key={institucion.id}>
      <td className="text-left">{institucion.codigo}</td>
      <td className="text-left">{institucion.nombre}</td>
      <td className="text-left">{institucion.ruc}</td>
      <td className="text-left">{institucion.abreviacion}</td>
      <td className="text-left">{institucion.creadorNombre}</td>
      <td>
        <Tooltip arrow title="Editar">
          <IconButton
            size="small"
            variant="outlined"
            color="secondary"
            component={Link}
            to={`${ROUTES.INSTITUCIONES_EDITAR.replace(
              ':id',
              institucion.id
            )}`}>
            <FontAwesomeIcon icon={['fa', 'edit']} />
          </IconButton>
        </Tooltip>
      </td>
      <td>
        <Tooltip arrow title="Eliminar">
          <IconButton
            size="small"
            variant="outlined"
            color="secondary"
            onClick={onDeleteInstitucion}>
            <FontAwesomeIcon icon={['fa', 'trash']} />
          </IconButton>
        </Tooltip>
      </td>
      <td className="text-center">
        <VerMas institucion={institucion} />
      </td>
    </tr>
  );
};
