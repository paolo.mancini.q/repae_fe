import React, { useEffect, useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Resource } from 'ghost-stories/dist/unions/Resource';
import { FIX_URL } from 'ghost-stories/dist/constants';
import { useParams, useHistory } from 'react-router-dom';

import {
  TipoInstitucion,
  EditarInstitucion,
  LimpiarInstitucion,
  ModificarInstitucion
} from '../../../redux/actions';

import FormularioInstitucion from '../FormInstitucion';

export default () => {
  const { id } = useParams();

  const history = useHistory();
  const dispatch = useDispatch();
  const formulario = useSelector((state) => state.instituciones.formulario);
  const tipoInstitucion = useSelector(
    (state) => state.parametros.tipo_institucion
  );

  const original = useSelector((state) =>
    state.instituciones.lista
      .map((xs) => xs.data.find((x) => x.id === id))
      .getDataOr(null)
  );

  useEffect(() => {
    dispatch(TipoInstitucion(tipoInstitucion.update()));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (original === null) {
      return history.goBack();
    }
    dispatch(EditarInstitucion(original));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [original]);

  const onChangeField = useCallback(
    (event) => {
      dispatch(
        EditarInstitucion({
          ...formulario,
          [event.target.name]: event.target.value
        })
      );
    },
    [formulario, dispatch]
  );

  const onChangeTipoInstitucion = useCallback(
    (event, tipo_institucion) => {
      dispatch(
        EditarInstitucion({
          ...formulario,
          tipo: tipo_institucion.id
        })
      );
    },
    [formulario, dispatch]
  );

  const onCancelar = () => {
    dispatch(LimpiarInstitucion({}));
    history.goBack();
  };

  const onGuardar = useCallback(
    (event) => {
      event.preventDefault();
      dispatch(
        ModificarInstitucion(Resource.Query({ [FIX_URL]: id, ...formulario }))
      );
      history.goBack();
    },
    [dispatch, history, id, formulario]
  );

  return (
    <FormularioInstitucion
      {...{
        title: 'Editar Institución',
        onGuardar,
        tipoInstitucion,
        formulario,
        onCancelar,
        onChangeTipoInstitucion,
        onChangeField
      }}
    />
  );
};
