import React from 'react';
import { Grid, Card, CardContent, TextField, Button } from '@material-ui/core';
import tipoInstitucionData from './Agregar/institucionesData';
import ResourceRender from 'ghost-stories/dist/react/resource';

import { OnQuery, OnError, OnEmpty } from '../ResourceHelpers';

export default (props) => {
  const {
    title,
    onGuardar,
    tipoInstitucion,
    formulario,
    onCancelar,
    onChangeTipoInstitucion,
    onChangeField
  } = props;

  return (
    <form onSubmit={onGuardar}>
      <Card className="card-box mb-4">
        <div className="card-header pr-2">
          <div className="card-header--title">
            <h6 className="font-size-lg mt-2 text-dark">{title}</h6>
          </div>
        </div>
        <CardContent>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={12}>
              <TextField
                fullWidth
                className="mb-3"
                label="Codigo"
                variant="outlined"
                name="codigo"
                value={formulario.codigo}
                onChange={onChangeField}
                required
              />
              <TextField
                fullWidth
                className="mb-3"
                label="Nombre"
                variant="outlined"
                name="nombre"
                value={formulario.nombre}
                onChange={onChangeField}
                required
              />
              <TextField
                fullWidth
                className="mb-3"
                label="Abreviación"
                variant="outlined"
                name="abreviacion"
                value={formulario.abreviacion}
                onChange={onChangeField}
                required
              />
              <TextField
                fullWidth
                className="mb-3"
                label="RUC"
                variant="outlined"
                name="ruc"
                value={formulario.ruc}
                onChange={onChangeField}
                required
              />
              <ResourceRender
                resource={tipoInstitucion}
                Data={tipoInstitucionData}
                Query={OnQuery}
                Empty={OnError}
                Error={OnEmpty}
                props={{ tipoInstitucion: formulario.tipo }}
                handlers={{ onChange: onChangeTipoInstitucion }}
              />
            </Grid>
          </Grid>
        </CardContent>
        <div className="card-footer p-3 text-right">
          <Button
            className="mr-3"
            size="small"
            color="primary"
            onClick={onCancelar}
            variant="contained">
            <span className="btn-wrapper--label">Cancelar</span>
          </Button>
          <Button
            size="small"
            color="secondary"
            type="submit"
            variant="contained">
            <span className="btn-wrapper--label">Guardar</span>
          </Button>
        </div>
      </Card>
    </form>
  );
};
