import React, { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import {
  AgregarInstitucion,
  EditarInstitucion,
  LimpiarInstitucion,
  TipoInstitucion
} from '../../../redux/actions';

import FormInstitucion from '../FormInstitucion';

export default () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const user = useSelector((state) => state.authentication.user);
  const userId = user.map((x) => x.additionalUserInfo.profile.id).getDataOr('');
  const userName = user
    .map((x) => x.additionalUserInfo.profile.name)
    .getDataOr('');

  useEffect(() => {
    dispatch(
      EditarInstitucion({
        ...formulario,
        creadorNombre: userName,
        creadorId: userId
      })
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  const tipoInstitucion = useSelector(
    (state) => state.parametros.tipo_institucion
  );
  useEffect(() => {
    dispatch(TipoInstitucion(tipoInstitucion.update()));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const formulario = useSelector((state) => state.instituciones.formulario);

  const onChangeField = useCallback(
    (event) => {
      dispatch(
        EditarInstitucion({
          ...formulario,
          [event.target.name]: event.target.value
        })
      );
    },
    [formulario, dispatch]
  );

  const onChangeTipoInstitucion = useCallback(
    (event, tipo) => {
      dispatch(EditarInstitucion({ ...formulario, tipo: tipo.id }));
    },
    [formulario, dispatch]
  );
  const onCancelar = () => {
    history.goBack();
    dispatch(LimpiarInstitucion({}));
  };

  const agregar = useSelector((state) => state.instituciones.agregar);

  const onGuardar = useCallback(
    (event) => {
      event.preventDefault();
      dispatch(AgregarInstitucion(agregar.update(formulario)));
      history.goBack();
    },
    [dispatch, agregar, formulario, history]
  );

  return (
    <FormInstitucion
      {...{
        title: 'Agregar Institución',
        tipoInstitucion,
        formulario,
        onChangeTipoInstitucion,
        onCancelar,
        onGuardar,
        onChangeField
      }}
    />
  );
};
