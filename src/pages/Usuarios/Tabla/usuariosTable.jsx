import React, { useState } from 'react';
import { TablePagination } from '@material-ui/core';
import Fila from './Fila';

export default ({ value }) => {
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [page, setPage] = useState(0);
  const handlePageChange = (event, page) => {
    setPage(page);
  };

  const handleRowsPerPageChange = (event) => {
    setRowsPerPage(event.target.value);
  };

  return (
    <div className="table-responsive">
      <table className="table table-striped table-hover text-nowrap mb-0">
        <thead>
          <tr>
            <th className="text-left">Nombres</th>
            <th className="text-left">Apellidos</th>
            <th className="text-left">Correo</th>
            <th className="text-left">Institución</th>
            <th className="text-left">Rol</th>
            <th className="text-center" colSpan="3">
              Acciones
            </th>
          </tr>
        </thead>

        <tbody>
          {value.data
            .slice(page * rowsPerPage, (page + 1) * rowsPerPage)
            .map((usuario) => (
              <Fila usuario={usuario} key={usuario.id} />
            ))}
        </tbody>
      </table>
      <TablePagination
        component="div"
        rowsPerPageOptions={[5, 10, 25]}
        count={value.data.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={handlePageChange}
        onChangeRowsPerPage={handleRowsPerPageChange}
      />
    </div>
  );
};
