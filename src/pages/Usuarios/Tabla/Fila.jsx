import React, { useCallback } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IconButton, Tooltip } from '@material-ui/core';
import { FIX_URL } from 'ghost-stories/dist/constants';
import { Resource } from 'ghost-stories/dist/unions/Resource';
import { EliminarUsuario } from '../../../redux/actions';
import { useDispatch } from 'react-redux';
import VerPerfil from './VerPerfil';
import ROUTES from '../../../constants/routes';
import { Link } from 'react-router-dom';

export default (props) => {
  const { usuario } = props;
  const dispatch = useDispatch();
  const onDeleteUsuario = useCallback(() => {
    dispatch(EliminarUsuario(Resource.Query({ [FIX_URL]: usuario.id })));
  }, [dispatch, usuario]);

  return (
    <tr key={usuario.id}>
      <td className="text-left">{usuario.nombre}</td>
      <td className="text-left">{usuario.apellido}</td>
      <td className="text-left">{usuario.correo}</td>
      <td className="text-left">{usuario.institucion}</td>

      <td className="text-left">
        <div className="badge badge-info px-4">{usuario.rol}</div>
      </td>

      <td>
        <Tooltip arrow title="Editar">
          <IconButton
            size="small"
            variant="outlined"
            color="secondary"
            component={Link}
            to={`${ROUTES.USUARIO_EDITAR.replace(':id', usuario.id)}`}>
            <FontAwesomeIcon icon={['fa', 'edit']} />
          </IconButton>
        </Tooltip>
      </td>

      <td className="text-center">
        <Tooltip arrow title="Eliminar">
          <IconButton
            size="small"
            variant="outlined"
            color="secondary"
            onClick={onDeleteUsuario}>
            <FontAwesomeIcon icon={['fa', 'trash']} />
          </IconButton>
        </Tooltip>
      </td>

      <td className="text-center">
        <VerPerfil usuario={usuario} />
      </td>
    </tr>
  );
};
