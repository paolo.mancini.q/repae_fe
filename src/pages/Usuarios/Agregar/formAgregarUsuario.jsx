import { Card, CardContent, Grid, TextField } from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import React from 'react';

const rolSelect = [
  { title: 'Evaluador' },
  { title: 'Invitado' },
  { title: 'Administrador' }
];

export default () => {
  return (
    <Card className="card-box mb-4">
      <div className="card-header pr-2">
        <div className="card-header--title">
          <h6 className="font-size-lg mt-2 text-dark">Agregar Usuario</h6>
        </div>
      </div>
      <CardContent>
        <form>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={2}></Grid>

            <Grid item xs={12} sm={8}>
              <TextField
                fullWidth
                className="mb-3"
                label="Nombre"
                variant="outlined"
              />
              <TextField
                fullWidth
                className="mb-3"
                label="Correo"
                variant="outlined"
                type="email"
              />
              <Autocomplete
                options={rolSelect}
                getOptionLabel={(option) => option.title}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label="Rol"
                    variant="outlined"
                    className="mb-3"
                    fullWidth
                  />
                )}
              />
            </Grid>

            <Grid item xs={12} sm={2}></Grid>
          </Grid>
        </form>
      </CardContent>
    </Card>
  );
};
