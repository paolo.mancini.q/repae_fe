import React from 'react';
import { Button, Card, CardContent, Grid, TextField } from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { ROLES } from '../../constants/parametros';

export default (props) => {
  const {
    onGuardar,
    formulario,
    onChangeNombre,
    onCancelar,
    onChangeRol,
    title,
    onChangeCorreo
  } = props;
  return (
    <form onSubmit={onGuardar}>
      <Card className="card-box mb-4">
        <div className="card-header pr-2">
          <div className="card-header--title">
            <h6 className="font-size-lg mt-2 text-dark">{title}</h6>
          </div>
        </div>
        <CardContent>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={2}></Grid>

            <Grid item xs={12} sm={8}>
              <TextField
                fullWidth
                className="mb-3"
                label="Nombre"
                variant="outlined"
                required
                value={formulario.nombre}
                onChange={onChangeNombre}
              />
              <TextField
                fullWidth
                name="correo"
                className="mb-3"
                label="Correo"
                variant="outlined"
                type="email"
                value={formulario.correo}
                onChange={onChangeCorreo}
              />
              <Autocomplete
                disableClearable
                options={ROLES}
                onChange={onChangeRol}
                value={formulario.rol}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label="Rol"
                    variant="outlined"
                    className="mb-3"
                    required
                    fullWidth
                  />
                )}
              />
            </Grid>

            <Grid item xs={12} sm={2}></Grid>
          </Grid>
        </CardContent>
        <div className="card-footer p-3 text-right">
          <Button
            className="mr-3"
            size="small"
            color="primary"
            onClick={onCancelar}
            variant="contained">
            <span className="btn-wrapper--label">Cancelar</span>
          </Button>
          <Button
            size="small"
            color="secondary"
            type="submit"
            variant="contained">
            <span className="btn-wrapper--label">Guardar</span>
          </Button>
        </div>
      </Card>
    </form>
  );
};
