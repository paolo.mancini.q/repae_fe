import React, { useState } from 'react';
import { Dialog, Tooltip, IconButton, Card, Divider } from '@material-ui/core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import ShortText from '../../Helpers/ShortText';
import ODSList from '../../Helpers/ODSList';

export default (props) => {
  const { lineamiento } = props;

  const [modal, setModal] = useState(false);
  const toggle = () => setModal((shown) => !shown);

  return (
    <div>
      <Tooltip arrow title="Ver más">
        <IconButton
          size="small"
          variant="outlined"
          color="secondary"
          onClick={toggle}>
          <FontAwesomeIcon icon={['fa', 'ellipsis-v']} />
        </IconButton>
      </Tooltip>
      <Dialog fullWidth open={modal} onClose={toggle}>
        <Card className="card-box p-4">
          <div>
            <div className="d-flex align-items-center mb-3">
              <div className="w-100">
                <span className="font-weight-bold font-size-lg">
                  {lineamiento.codigo} - {lineamiento.nombre}
                </span>
              </div>
            </div>
            <Divider />
            <div className="my-4 font-size-sm p-3 bg-secondary rounded-sm">
              <ShortText titulo="Descripcion" valor={lineamiento.descripcion} />
              <ODSList titulo="ODSs Vinculados" valor={lineamiento.ods} />
            </div>
          </div>
        </Card>
      </Dialog>
    </div>
  );
};
