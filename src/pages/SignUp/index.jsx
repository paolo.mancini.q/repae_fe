import React from 'react';
import { SignUpPage } from '../../components/SignUp';
import { useDispatch } from 'react-redux';
import {
  LoginWithFacebook,
  LoginWithGoogle,
  LoginWithTwitter
} from '../../redux/actions';

export default () => {
  const dispatch = useDispatch();
  const onGoogleSignUp = () => {
    dispatch(LoginWithGoogle());
  };
  const onFacebookSignup = () => {
    dispatch(LoginWithFacebook());
  };
  const onTwitterSignup = () => {
    dispatch(LoginWithTwitter());
  };
  return (
    <SignUpPage {...{ onFacebookSignup, onGoogleSignUp, onTwitterSignup }} />
  );
};
