import React, { useCallback, useEffect } from 'react';
import { useHistory } from 'react-router-dom';

import { useSelector, useDispatch } from 'react-redux';
import {
  AgregarIndicador,
  EditarIndicador,
  Instituciones,
  LimpiarIndicador
} from '../../../redux/actions';

import FormularioIndicadores from '../FormIndicadores';

export default () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const agregar = useSelector((state) => state.indicadores.agregar);
  const formulario = useSelector((state) => state.indicadores.formulario);
  const instituciones = useSelector((state) => state.instituciones.lista);
  const user = useSelector((state) => state.authentication.user);
  const userId = user.map((x) => x.additionalUserInfo.profile.id).getDataOr('');
  const userName = user
    .map((x) => x.additionalUserInfo.profile.name)
    .getDataOr('');

  useEffect(() => {
    dispatch(
      EditarIndicador({
        ...formulario,
        creadorNombre: userName,
        creadorId: userId
      })
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    dispatch(Instituciones(instituciones.update()));

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const onChageActivo = useCallback(
    (event) => {
      dispatch(
        EditarIndicador({ ...formulario, activo: event.target.checked })
      );
    },
    [formulario, dispatch]
  );

  const onChangeCodigo = useCallback(
    (event) => {
      dispatch(EditarIndicador({ ...formulario, codigo: event.target.value }));
    },
    [formulario, dispatch]
  );

  const onChangeNombre = useCallback(
    (event) => {
      dispatch(EditarIndicador({ ...formulario, nombre: event.target.value }));
    },
    [formulario, dispatch]
  );

  const onChangeUnidadMedida = useCallback(
    (event) => {
      dispatch(
        EditarIndicador({ ...formulario, unidadMedida: event.target.value })
      );
    },
    [formulario, dispatch]
  );

  const onChangeFormulas = useCallback(
    (event) => {
      dispatch(EditarIndicador({ ...formulario, formula: event.target.value }));
    },
    [formulario, dispatch]
  );

  const onChangeFrecuencia = useCallback(
    (event) => {
      dispatch(
        EditarIndicador({ ...formulario, frecuencia: event.target.value })
      );
    },
    [formulario, dispatch]
  );

  const onChangeDescripcion = useCallback(
    (event) => {
      dispatch(
        EditarIndicador({ ...formulario, descripcion: event.target.value })
      );
    },
    [formulario, dispatch]
  );

  const onChangeInstitucion = useCallback(
    (event, institucion) => {
      dispatch(EditarIndicador({ ...formulario, institucion: institucion.id }));
    },
    [formulario, dispatch]
  );

  const onChangeTendencia = useCallback(
    (event, tendencia) => {
      dispatch(EditarIndicador({ ...formulario, tendencias: tendencia }));
    },
    [formulario, dispatch]
  );

  const onCancelar = useCallback(() => {
    dispatch(LimpiarIndicador({}));
    history.goBack();
  }, [dispatch, history]);

  const onGuardar = useCallback(
    (event) => {
      event.preventDefault();
      dispatch(AgregarIndicador(agregar.update(formulario)));
      history.goBack();
    },
    [dispatch, agregar, formulario, history]
  );

  const onAgregarInsitucion = useCallback(() => {
    history.goBack();
  }, [history]);
  return (
    <FormularioIndicadores
      {...{
        formulario,
        instituciones,
        onChageActivo,
        onChangeCodigo,
        onChangeNombre,
        onChangeUnidadMedida,
        onChangeFormulas,
        onChangeFrecuencia,
        onChangeDescripcion,
        onChangeInstitucion,
        onChangeTendencia,
        onCancelar,
        onGuardar,
        onAgregarInsitucion
      }}
    />
  );
};
