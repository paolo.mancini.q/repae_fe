import React, { Fragment, useCallback, useState } from 'react';
import {
  Dialog,
  Button,
  Grid,
  TextField,
  Card,
  CardContent
} from '@material-ui/core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useDispatch, useSelector } from 'react-redux';
import { EditarIndicador } from '../../../../redux/actions';

export default () => {
  const [modal1, setModal1] = useState(false);
  const [titulo, setTitulo] = useState('');
  const [url, setURL] = useState('');
  const toggle1 = () => setModal1(!modal1);
  const dispatch = useDispatch();
  const formulario = useSelector((state) => state.indicadores.formulario);

  const onChangeReferenciasTitulo = useCallback((event) => {
    setTitulo(event.target.value);
  });

  const onChangeReferenciasURL = useCallback((event) => {
    setURL(event.target.value);
  });

  const onAgregarReferencia = useCallback(() => {
    dispatch(
      EditarIndicador(
        Object.assign({}, formulario, {
          referencias: formulario.referencias.concat([{ titulo, url }])
        })
      )
    );
    toggle1();
    setTitulo('');
    setURL('');
  });

  return (
    <Fragment>
      <div className="card-footer p-3 text-right">
        <Button
          onClick={toggle1}
          size="small"
          color="secondary"
          variant="contained">
          <span className="btn-wrapper--label">Agregar</span>
          <span className="btn-wrapper--icon">
            <FontAwesomeIcon icon={['fas', 'plus']} />
          </span>
        </Button>
      </div>
      <Dialog fullWidth open={modal1} onClose={toggle1}>
        <Card className="card-box">
          <div className="card-header-alt d-flex justify-content-between p-4">
            <h6 className="font-weight-bold font-size-lg mb-1 text-black">
              Agregar Referencias
            </h6>
          </div>
          <CardContent>
            <form>
              <Grid container spacing={2}>
                <Grid item xs={12} sm={2}></Grid>

                <Grid item xs={12} sm={8}>
                  <TextField
                    className="mb-3"
                    label="Título"
                    variant="outlined"
                    value={titulo}
                    onChange={onChangeReferenciasTitulo}
                    fullWidth
                  />
                  <TextField
                    className="mb-3"
                    label="URL"
                    multiline
                    rows="5"
                    variant="outlined"
                    value={url}
                    onChange={onChangeReferenciasURL}
                    fullWidth
                  />
                </Grid>

                <Grid item xs={12} sm={2}></Grid>
              </Grid>
            </form>
          </CardContent>
          <div className="card-footer p-3 text-right">
            <Button
              onClick={onAgregarReferencia}
              size="small"
              color="secondary"
              variant="contained">
              <span className="btn-wrapper--label">Aceptar</span>
            </Button>
          </div>
        </Card>
      </Dialog>
    </Fragment>
  );
};
