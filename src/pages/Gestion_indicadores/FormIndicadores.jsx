import React from 'react';
import AddIcon from '@material-ui/icons/Add';
import Autocomplete from '@material-ui/lab/Autocomplete';
import ResourceRender from 'ghost-stories/dist/react/resource';

import {
  Grid,
  Card,
  TextField,
  CardContent,
  FormControlLabel,
  Switch,
  Button
} from '@material-ui/core';

import TabalOdsIndicadores from './Agregar/components/tablaOdsIndicadores';
import TabalRefIndicadores from './Agregar/components/tablaRefIndicadores';
import InstitucionesData from './Agregar/InstitucionesData';
import { TENDENCIA_INDICADOR } from '../../constants/parametros';

import { OnQuery, OnError, OnEmpty } from '../ResourceHelpers';

export default (props) => {
  const {
    formulario,
    instituciones,
    onChageActivo,
    onChangeCodigo,
    onChangeNombre,
    onChangeUnidadMedida,
    onChangeFormulas,
    onChangeFrecuencia,
    onChangeDescripcion,
    onChangeInstitucion,
    onChangeTendencia,
    onCancelar,
    onGuardar,
    onAgregarInsitucion
  } = props;

  return (
    <form onSubmit={onGuardar}>
      <Card className="card-box mb-4">
        <div className="card-header pr-2">
          <div className="card-header--title">
            <h6 className="font-size-lg mt-2 text-dark">Agregar Indicadores</h6>
          </div>
        </div>
        <CardContent>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={2}></Grid>

            <Grid item xs={12} sm={8}>
              <TextField
                fullWidth
                className="mb-3"
                label="Codigo"
                variant="outlined"
                value={formulario.codigo}
                onChange={onChangeCodigo}
                required
              />
              <TextField
                fullWidth
                className="mb-3"
                label="Nombre"
                variant="outlined"
                value={formulario.nombre}
                onChange={onChangeNombre}
                required
              />
              <TextField
                className="mb-3"
                label="Descripción"
                multiline
                rows="5"
                variant="outlined"
                value={formulario.descripcion}
                onChange={onChangeDescripcion}
                fullWidth
                required
              />

              <TextField
                fullWidth
                className="mb-3"
                label="Unidad de medida"
                variant="outlined"
                value={formulario.unidadMedida}
                onChange={onChangeUnidadMedida}
                required
              />
              <TextField
                className="mb-3"
                label="Fórmula y método de cálculo"
                multiline
                rows="5"
                variant="outlined"
                value={formulario.formula}
                onChange={onChangeFormulas}
                fullWidth
                required
              />
              <TextField
                fullWidth
                className="mb-3"
                label="Frecuencia"
                variant="outlined"
                value={formulario.frecuencia}
                onChange={onChangeFrecuencia}
                required
              />
            </Grid>

            <Grid item xs={12} sm={2}></Grid>
            <Grid item xs={12} sm={2}></Grid>

            <Grid item xs={12} sm={6}>
              <ResourceRender
                resource={instituciones}
                Data={InstitucionesData}
                Query={OnQuery}
                Empty={OnEmpty}
                Error={OnError}
                props={{ institucion: formulario.institucion }}
                handlers={{ onChange: onChangeInstitucion }}
              />
            </Grid>
            <Grid item xs={12} sm={2}>
              <Button
                className="mt-2 ml-2"
                variant="contained"
                color="secondary"
                onClick={onAgregarInsitucion}
                startIcon={<AddIcon />}>
                Agregar
              </Button>
            </Grid>
            <Grid item xs={12} sm={2}></Grid>

            <Grid item xs={12} sm={2}></Grid>
            <Grid item xs={12} sm={8}>
              <Autocomplete
                disableClearable
                options={TENDENCIA_INDICADOR}
                value={formulario.tendencias}
                onChange={onChangeTendencia}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label="Tendencia"
                    variant="outlined"
                    className="mb-3"
                    fullWidth
                    required
                  />
                )}
              />
              <TabalOdsIndicadores />
              <TabalRefIndicadores />
              <FormControlLabel
                control={
                  <Switch
                    name="activo"
                    checked={formulario.activo}
                    onChange={onChageActivo}
                  />
                }
                label="Activo"
                className="mb-3"
              />
            </Grid>

            <Grid item xs={12} sm={2}></Grid>
          </Grid>
        </CardContent>
        <div className="card-footer p-3 text-right">
          <Button
            className="mr-3"
            size="small"
            color="primary"
            onClick={onCancelar}
            variant="contained">
            <span className="btn-wrapper--label">Cancelar</span>
          </Button>
          <Button
            size="small"
            color="secondary"
            type="submit"
            variant="contained">
            <span className="btn-wrapper--label">Guardar</span>
          </Button>
        </div>
      </Card>
    </form>
  );
};
