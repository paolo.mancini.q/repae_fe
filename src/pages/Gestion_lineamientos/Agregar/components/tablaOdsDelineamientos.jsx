import React from 'react';
import { Card, Grid, CardContent } from '@material-ui/core';
import FormAgregarOdsDelineamientos from './formAgregarOdsDelineamientos';
import { useSelector } from 'react-redux';
import OdsCard from './odsCard';

export default () => {
  const ods = useSelector((state) => state.delineamientos.formulario.ods);

  return (
    <Grid item xs={12}>
      <Card className="mb-4">
        <div className="card-header">
          <div className="card-header--title font-size-md font-weight-bold py-2">
            ODS
          </div>
        </div>
        <CardContent>
          {ods.map((ods, i) => (
            <OdsCard key={i.toString()} ods={ods} indice={i} />
          ))}
        </CardContent>
        <FormAgregarOdsDelineamientos />
      </Card>
    </Grid>
  );
};
