import React, { useCallback } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  FormControl,
  FormControlLabel,
  FormGroup,
  IconButton,
  Tooltip,
  Switch
} from '@material-ui/core';
import {
  EliminarLineamiento,
  ModificarLineamiento
} from '../../../redux/actions';
import { Resource } from 'ghost-stories/dist/unions/Resource';
import { FIX_URL } from 'ghost-stories/dist/constants';
import { useDispatch } from 'react-redux';
import VerMas from './VerMas';
import { Link } from 'react-router-dom';
import ROUTES from '../../../constants/routes';

export default (props) => {
  const { lineamiento } = props;
  const dispatch = useDispatch();
  const onDeleteLineamiento = useCallback(() => {
    dispatch(
      EliminarLineamiento(Resource.Query({ [FIX_URL]: lineamiento.id }))
    );
  }, [dispatch, lineamiento]);

  const onChangeVisibilidad = useCallback(
    (event) => {
      event.preventDefault();
      const updatedInstitucion = {
        ...lineamiento,
        publico: !lineamiento.publico
      };
      dispatch(
        ModificarLineamiento(
          Resource.Query({ [FIX_URL]: lineamiento.id, ...updatedInstitucion })
        )
      );
    },
    [dispatch, lineamiento]
  );

  return (
    <tr key={lineamiento.id}>
      <td className="text-left">{lineamiento.codigo}</td>
      <td className="text-left">{lineamiento.nombre}</td>
      <td className="text-left">{lineamiento.descripcion}</td>
      <td className="text-left">{lineamiento.creadorNombre}</td>
      <td>
        <FormControl component="fieldset">
          <FormGroup aria-label="position" row>
            <FormControlLabel
              value="publico"
              control={
                <Switch
                  size="small"
                  color="secondary"
                  onChange={onChangeVisibilidad}
                  checked={lineamiento.publico}
                />
              }
              label="Público"
              labelPlacement="end"
            />
          </FormGroup>
        </FormControl>
      </td>
      <td>
        <Tooltip arrow title="Editar">
          <IconButton
            size="small"
            variant="outlined"
            color="secondary"
            component={Link}
            to={`${ROUTES.GESTION_LINEAMIENTOS_EDITAR.replace(
              ':id',
              lineamiento.id
            )}`}>
            <FontAwesomeIcon icon={['fa', 'edit']} />
          </IconButton>
        </Tooltip>
      </td>

      <td className="text-center">
        <Tooltip arrow title="Eliminar">
          <IconButton
            size="small"
            variant="outlined"
            color="secondary"
            onClick={onDeleteLineamiento}>
            <FontAwesomeIcon icon={['fa', 'trash']} />
          </IconButton>
        </Tooltip>
      </td>

      <td className="text-center">
        <VerMas lineamiento={lineamiento} />
      </td>
    </tr>
  );
};
