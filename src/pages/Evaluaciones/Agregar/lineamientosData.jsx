import React from 'react';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { TextField } from '@material-ui/core';

export default ({ value, handlers, lineamiento }) => {
  const lineamientoValue = value.data.find((x) => x.id === lineamiento);

  return (
    <Autocomplete
      disableClearable
      options={value.data}
      getOptionLabel={(option) => option.nombre}
      onChange={handlers.onChange}
      value={lineamientoValue}
      renderInput={(params) => (
        <TextField
          {...params}
          label="Lineamiento evaluado"
          variant="outlined"
          className="mb-3"
          required
          fullWidth
        />
      )}
    />
  );
};
