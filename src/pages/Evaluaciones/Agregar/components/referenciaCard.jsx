import React, { useCallback } from 'react';
import { Box, Card, IconButton } from '@material-ui/core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { EditarEvaluacion } from '../../../../redux/actions';
import { useDispatch, useSelector } from 'react-redux';

export default (props) => {
  const { referencia, indice } = props;
  const dispatch = useDispatch();
  const formulario = useSelector((state) => state.evaluaciones.formulario);
  const onEliminarReferencia = useCallback(() => {
    dispatch(
      EditarEvaluacion(
        Object.assign({}, formulario, {
          referencias: formulario.referencias.filter(
            (value, index) => index !== indice
          )
        })
      )
    );
  });
  return (
    <Card className="card-box mb-4">
      <div className="card-header-alt d-flex justify-content-between p-4">
        <div className="d-flex justify-content-flex-start">
          <h6 className="font-weight-bold font-size-md mb-1 text-black">
            {referencia.titulo}:
          </h6>
          <span className="ml-2">{referencia.url}</span>
        </div>
        <Box className="d-flex align-items-center">
          <IconButton
            size="small"
            color="secondary"
            onClick={onEliminarReferencia}>
            <FontAwesomeIcon icon={['fas', 'trash']} />
          </IconButton>
        </Box>
      </div>
    </Card>
  );
};
