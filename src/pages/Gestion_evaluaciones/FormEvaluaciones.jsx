import React from 'react';
import Autocomplete from '@material-ui/lab/Autocomplete';
import AddIcon from '@material-ui/icons/Add';
import ResourceRender from 'ghost-stories/dist/react/resource';
import { Grid, Card, CardContent, TextField, Button } from '@material-ui/core';
import { Link } from 'react-router-dom';

import { ESTADOS_EVALUACIONES } from '../../constants/parametros';
import TablaAgregarIndicadores from './Agregar/components/tablaAgregarIndicadores';

import Editor from './Agregar/components/editor';
import lineamientosData from './Agregar/lineamientosData';
import TablaRefEvaluaciones from './Agregar/components/tablaRefGestionEvaluaciones';
import { OnQuery, OnError, OnEmpty } from '../ResourceHelpers';
import ROUTES from '../../constants/routes';

export default (props) => {
  const {
    onGuardar,
    formulario,
    parametros,
    onChangeField,
    onCancelar,
    onChangeIndicadores,
    delineamientos,
    onChangeDelineamiento,
    onChangeEstado,
    onChangeResumen,
    title
  } = props;

  return (
    <form onSubmit={onGuardar}>
      <Card className="card-box mb-4">
        <div className="card-header pr-2">
          <div className="card-header--title">
            <h6 className="font-size-lg mt-2 text-dark">{title}</h6>
          </div>
        </div>
        <CardContent>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={12}>
              <TextField
                fullWidth
                name="titulo"
                className="mb-3"
                label="Titulo de la evaluación"
                variant="outlined"
                required
                value={formulario.titulo}
                onChange={onChangeField}
              />
              <TextField
                fullWidth
                label="Marco de la evaluación (Proyecto)"
                variant="outlined"
                required
                name="marco"
                value={formulario.marco}
                onChange={onChangeField}
              />
            </Grid>

            <Grid item xs={12} sm={10}>
              <ResourceRender
                resource={delineamientos}
                Data={lineamientosData}
                Query={OnQuery}
                Empty={OnError}
                Error={OnEmpty}
                props={{ lineamiento: formulario.lineamiento }}
                handlers={{ onChange: onChangeDelineamiento }}
              />
            </Grid>
            <Grid item xs={12} sm={2}>
              <Button
                fullWidth
                className="mt-2 ml-2"
                variant="contained"
                color="secondary"
                component={Link}
                to={ROUTES.DELINEAMIENTOS_AGREGAR}
                startIcon={<AddIcon />}>
                Agregar
              </Button>
            </Grid>

            <Grid item xs={12} sm={12}>
              <Autocomplete
                disableClearable
                options={ESTADOS_EVALUACIONES}
                onChange={onChangeEstado}
                value={formulario.estado}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label="Estado"
                    variant="outlined"
                    className="mb-3"
                    required
                    fullWidth
                  />
                )}
              />
              <Editor
                className="mb-3"
                value={formulario.resumen}
                onChange={onChangeResumen}
              />
              <ResourceRender
                resource={parametros}
                Data={TablaAgregarIndicadores}
                Query={OnQuery}
                Empty={OnError}
                Error={OnEmpty}
                props={{ initialData: formulario.indicadores }}
                handlers={{ onChange: onChangeIndicadores }}
              />
            </Grid>
            <Grid item xs={12} sm={12}>
              <TablaRefEvaluaciones />
            </Grid>
          </Grid>
        </CardContent>
        <div className="card-footer p-3 text-right">
          <Button
            className="mr-3"
            size="small"
            color="primary"
            onClick={onCancelar}
            variant="contained">
            <span className="btn-wrapper--label">Cancelar</span>
          </Button>
          <Button
            size="small"
            color="secondary"
            type="submit"
            variant="contained">
            <span className="btn-wrapper--label">Guardar</span>
          </Button>
        </div>
      </Card>
    </form>
  );
};
