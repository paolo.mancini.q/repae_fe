import React from 'react';

import { Card, CardContent } from '@material-ui/core';

import { Editor } from 'react-draft-wysiwyg';

export default (props) => {
  const { value, onChange } = props;
  return (
    <Card
      className="card-box mb-4-spacing overflow-visible"
      style={{ minHeight: '400px' }}>
      <div className="card-header">
        <div className="card-header--title font-size-md font-weight-bold py-2">
          Resumen *
        </div>
      </div>
      <CardContent>
        <Editor
          placeholder="Resumen *"
          initialContentState={value}
          onContentStateChange={onChange}
          localization={{
            locale: 'es'
          }}
        />
      </CardContent>
    </Card>
  );
};
