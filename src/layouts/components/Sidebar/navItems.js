import ConfirmationNumberIcon from '@material-ui/icons/ConfirmationNumber';
import PeopleAltIcon from '@material-ui/icons/PeopleAlt';
import PieChartIcon from '@material-ui/icons/PieChart';
import PersonIcon from '@material-ui/icons/PersonOutlined';
import ReceiptIcon from '@material-ui/icons/ReceiptOutlined';
import EmojiObjectsIcon from '@material-ui/icons/EmojiObjects';
import VpnKeyIcon from '@material-ui/icons/VpnKey';

// cambiar rutas para usar desde la constante ROUTES
export default [
  {
    content: [
      {
        label: 'Evaluador',
        icon: ReceiptIcon,
        content: [
          {
            label: 'Mis Evaluaciones',
            icon: ConfirmationNumberIcon,
            to: '/evaluaciones'
          },
          {
            label: 'Indicadores',
            icon: PieChartIcon,
            to: '/indicadores'
          },
          {
            label: 'Instituciones',
            icon: PeopleAltIcon,
            to: '/instituciones'
          },
          {
            label: 'Lineamientos',
            icon: EmojiObjectsIcon,
            to: '/delineamientos'
          }
        ]
      },
      {
        label: 'Administrador',
        icon: VpnKeyIcon,
        content: [
          {
            label: 'Gestión de Indicadores',
            icon: PieChartIcon,
            to: '/gestion_indicadores'
          },
          {
            label: 'Gestión de Usuarios',
            icon: PersonIcon,
            to: '/usuarios'
          },
          {
            label: 'Gestión de Instituciones',
            icon: PeopleAltIcon,
            to: '/gestion_instituciones'
          },
          {
            label: 'Gestión de Evaluaciones',
            icon: ConfirmationNumberIcon,
            to: '/gestion_evaluaciones'
          },
          {
            label: 'Gestión de Lineamientos',
            icon: EmojiObjectsIcon,
            to: '/gestion_lineamientos'
          }
        ]
      }
    ]
  }
];
