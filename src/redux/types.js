export const User = () => {
  uuid: string;
  active: boolean;
  name: string;
  phone: string;
  email: string;
  roles: RoleType[''];
};

export const Region = () => {
  uiid: string;
  code: string;
  name: string;
};

export const ODS = () => {
  uuid: string;
  target: string;
  name: string;
  number: string; // eg 5.1.1
  parent: null | ODS;
};

export const Sex = () => {
  uuid: string;
  name: string;
};

export const AgeGroup = () => {
  uuid: string;
  name: string;
  from: number;
  to: number;
};

export const Point = () => {
  longitude: number;
  latitude: number;
};

export const Institution = () => {
  uuid: string;
  name: string;
  ruc: string;
  type: string;
};

export const SummaryStatus = () => {
  'draft' | 'evaluating' | 'finished' | 'published';
};

export const Summary = () => {
  uuid: string;
  active: boolean;
  title: string;
  abstract: string;
  project: string;

  status: SummaryStatus;
  location: Point;
  references: Reference[''];
  files: File[''];

  region: Region;
  demography: Demography;
  owner: User;
}

export const RoleType = () => { 
  'administrator' | 'manager' | 'evaluator' | '';
};

export const Reference = () => {
  uuid: string;
  url: string;
}

export const File = () => {
  uuid: string;
  url: string;
  name: string;
}

export const Demography = () => {
  uuid: string;
  name: string;
}
