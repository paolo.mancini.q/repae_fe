export const mapGestionEvaluaciones = (data) => {
  return {
    id: data.id,
    estado: data.estado,
    titulo: data.titulo,
    proyecto: data.proyecto,
    tipo: data.tipo
  };
};
