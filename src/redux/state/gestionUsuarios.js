export const mapGestionUsuarios = (data) => {
  return {
    id: data.id,
    nombre: data.nombre,
    correo: data.correo,
    rol: data.rol
  };
};
