export const mapGestionIndicadores = (data) => {
  return {
    id: data.id,
    descripcion: data.descripcion,
    codigo: data.codigo,
    nombre: data.nombre,
    institucion: data.institucion,
    responsable: data.responsable
  };
};
