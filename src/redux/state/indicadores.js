export const mapIndicadores = (data) => {
  return {
    id: data.id,
    descripcion: data.descripcion,
    codigo: data.codigo,
    nombre: data.nombre,
    institucion: data.institucion,
    valor_referencia: data.valor_referencia
  };
};
