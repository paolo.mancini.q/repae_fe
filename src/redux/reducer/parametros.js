import { merge } from 'remeda';
import {
  SEXO,
  TIPO_INSTITUCION,
  TIPO_LINEAMIENTO,
  REGION,
  RANGO_ETARIO
} from '../actionTypes';
import { Resource } from 'ghost-stories/dist/unions/Resource';

const initial = {
  sexo: Resource.Empty({}),
  region: Resource.Empty({}),
  rango_etario: Resource.Empty({}),
  tipo_institucion: Resource.Empty({}),
  tipo_lineamiento: Resource.Empty({})
};

export default (state = initial, action) => {
  switch (action.type) {
    case SEXO:
      return merge(state, { sexo: action.payload });
    case REGION:
      return merge(state, { region: action.payload });
    case RANGO_ETARIO:
      return merge(state, { rango_etario: action.payload });
    case TIPO_INSTITUCION:
      return merge(state, {
        tipo_institucion: action.payload
      });
    case TIPO_LINEAMIENTO:
      return merge(state, {
        tipo_lineamiento: action.payload
      });
    default:
      return state;
  }
};
