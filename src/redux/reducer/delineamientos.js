import { merge } from 'remeda';
import {
  AGREGAR_LINEAMIENTO,
  DELINEAMIENTOS,
  EDITAR_LINEAMIENTO,
  LIMPIAR_LINEAMIENTO
} from '../actionTypes';
import { Resource } from 'ghost-stories/dist/unions/Resource';

const initial = {
  lista: Resource.Empty({}),
  agregar: Resource.Empty({}),
  formulario: {
    codigo: '',
    nombre: '',
    tipo: '',
    institucion: '',
    descripcion: '',
    ods: [],
    creadorId: '',
    creadorNombre: '',
    publico: false
  }
};

export default (state = initial, action) => {
  switch (action.type) {
    case DELINEAMIENTOS:
      return merge(state, { lista: action.payload });
    case AGREGAR_LINEAMIENTO:
      return merge(state, { agregar: action.payload });
    case EDITAR_LINEAMIENTO:
      return merge(state, { formulario: action.payload });
    case LIMPIAR_LINEAMIENTO:
      return merge(state, { formulario: initial.formulario });
    default:
      return state;
  }
};
