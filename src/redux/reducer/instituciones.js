import { merge } from 'remeda';
import {
  AGREGAR_INSTITUCION,
  EDITAR_INSTITUCION,
  INSTITUCIONES,
  LIMPIAR_INSTITUCION
} from '../actionTypes';
import { Resource } from 'ghost-stories/dist/unions/Resource';

const initial = {
  lista: Resource.Empty({}),
  agregar: Resource.Empty({}),
  formulario: {
    codigo: '',
    nombre: '',
    abreviacion: '',
    ruc: '',
    tipo: '',
    creadorId: '',
    creadorNombre: '',
    publico: false
  }
};

export default (state = initial, action) => {
  switch (action.type) {
    case INSTITUCIONES:
      return merge(state, { lista: action.payload });
    case AGREGAR_INSTITUCION:
      return merge(state, { agregar: action.payload });
    case EDITAR_INSTITUCION:
      return merge(state, { formulario: action.payload });
    case LIMPIAR_INSTITUCION:
      return merge(state, { formulario: initial.formulario });
    default:
      return state;
  }
};
