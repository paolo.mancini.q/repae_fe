import { merge } from 'remeda';
import { Resource } from 'ghost-stories/dist/unions/Resource';
import * as ActionTypes from '../actionTypes';
const initialState = {
  user: Resource.Empty({})
};
const reducer = (state = initialState, action) => {
  switch (action.type) {
    case ActionTypes.LOGIN:
      return merge(state, { user: action.payload });
    default:
      return state;
  }
};
export default reducer;
