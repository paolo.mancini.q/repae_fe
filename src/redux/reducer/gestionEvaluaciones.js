import { merge } from 'remeda';
import { GESTION_EVALUACIONES } from '../actionTypes';
import { Resource } from 'ghost-stories/dist/unions/Resource';


const initial = {
    gestionEvaluaciones: Resource.Empty({}),
  };

export default (state = initial , action) => {
  switch (action.type) {
    case GESTION_EVALUACIONES:
      return merge(state.gestionEvaluaciones, { gestionEvaluaciones: action.payload });
    default:
      return state;
  }
};