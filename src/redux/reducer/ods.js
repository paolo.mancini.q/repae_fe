import { merge } from 'remeda';
import { ODS } from '../actionTypes';
import { Resource } from 'ghost-stories/dist/unions/Resource';


const initial = {
    ods: Resource.Empty({}),
  };

export default (state = initial , action) => {
  switch (action.type) {
    case ODS:
      return merge(state.ods, { ods: action.payload });
    default:
      return state;
  }
};