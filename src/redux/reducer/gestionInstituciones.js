import { merge } from 'remeda';
import { GESTION_INSTITUCIONES } from '../actionTypes';
import { Resource } from 'ghost-stories/dist/unions/Resource';

const initial = {
  gestionInstituciones: Resource.Empty({})
};

export default (state = initial, action) => {
  switch (action.type) {
    case GESTION_INSTITUCIONES:
      return merge(state.gestionInstituciones, {
        gestionInstituciones: action.payload
      });
    default:
      return state;
  }
};
