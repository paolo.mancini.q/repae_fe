import { merge } from 'remeda';
import {
  AGREGAR_INDICADOR,
  EDITAR_INDICADOR,
  INDICADORES,
  LIMPIAR_INDICADOR
} from '../actionTypes';
import { Resource } from 'ghost-stories/dist/unions/Resource';

const initial = {
  lista: Resource.Empty({}),
  agregar: Resource.Empty({}),
  formulario: {
    activo: true,
    codigo: '',
    descripcion: '',
    formula: '',
    frecuencia: '',
    institucion: '',
    nombre: '',
    ods: [],
    referencias: [],
    tendencias: '',
    unidadMedida: '',
    creadorId: '',
    creadorNombre: '',
    publico: false
  }
};

export default (state = initial, action) => {
  switch (action.type) {
    case INDICADORES:
      return merge(state, { lista: action.payload });
    case AGREGAR_INDICADOR:
      return merge(state, { agregar: action.payload });
    case EDITAR_INDICADOR:
      return merge(state, { formulario: action.payload });
    case LIMPIAR_INDICADOR:
      return merge(state, { formulario: initial.formulario });
    default:
      return state;
  }
};
