import { combineReducers } from 'redux';
import authentication from './authentication';
import ThemeOptions from './ThemeOptions';
import instituciones from './instituciones';
import ods from './ods';
import evaluaciones from './evaluaciones';
import indicadores from './indicadores';
import delineamientos from './delineamientos';
import gestionIndicadores from './gestionIndicadores';
import gestionInstituciones from './gestionInstituciones';
import gestionEvaluaciones from './gestionEvaluaciones';
import gestionUsuarios from './gestionUsuarios';
import parametros from './parametros';
import snackBar from './snackBar';

export default combineReducers({
  authentication,
  ThemeOptions,
  instituciones,
  ods,
  evaluaciones,
  indicadores,
  delineamientos,
  gestionIndicadores,
  gestionInstituciones,
  gestionEvaluaciones,
  gestionUsuarios,
  parametros,
  snackBar
});
