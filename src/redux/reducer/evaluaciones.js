import { merge } from 'remeda';
import {
  AGREGAR_EVALUACION,
  EVALUACIONES,
  EDITAR_EVALUACION,
  LIMPIAR_EVALUACION
} from '../actionTypes';
import { Resource } from 'ghost-stories/dist/unions/Resource';
import { ESTADO_BORRADOR } from '../../constants/parametros';

const initial = {
  lista: Resource.Empty({}),
  agregar: Resource.Empty({}),
  formulario: {
    titulo: '',
    marco: '',
    lineamiento: '',
    resumen: '',
    indicadores: [],
    referencias: [],
    activo: '',
    estado: ESTADO_BORRADOR
  }
};

export default (state = initial, action) => {
  switch (action.type) {
    case EVALUACIONES:
      return merge(state, { lista: action.payload });
    case AGREGAR_EVALUACION:
      return merge(state, { agregar: action.payload });
    case EDITAR_EVALUACION:
      return merge(state, { formulario: action.payload });
    case LIMPIAR_EVALUACION:
      return merge(state, { formulario: initial.formulario });
    default:
      return state;
  }
};
