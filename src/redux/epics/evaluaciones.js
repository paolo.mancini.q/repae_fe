import {
  EVALUACIONES,
  AGREGAR_EVALUACION,
  ELIMINAR_EVALUACION,
  MODIFICAR_EVALUACION
} from '../actionTypes';
import {
  obtenerEvaluaciones,
  putEvaluacion,
  postEvaluacion,
  deleteEvaluacion
} from '../api';
import {
  Evaluaciones,
  AgregarEvaluacion,
  EliminarEvaluacion,
  MostrarMensaje,
  ModificarEvaluacion,
  LimpiarEvaluacion
} from '../actions';
import { toParams, makeRequest } from 'ghost-stories/dist/streams/resource';
import { filter, map, mergeMap } from 'rxjs/operators';
import { ofType, combineEpics } from 'redux-observable';
import { Resource } from 'ghost-stories/dist/unions/Resource';
import { addAuth } from '../api/streamHelpers';

const solicitarEvaluaciones = (action$) =>
  action$.pipe(
    ofType(EVALUACIONES),
    toParams,
    addAuth(),
    makeRequest(obtenerEvaluaciones),
    map(Evaluaciones)
  );

// const limpiarFormulario = (action$) =>
//   action$.pipe(
//     ofType(EVALUACIONES),
//     map(() => LimpiarEvaluacion())
//   );

const guardarEvaluaciones = (action$) =>
  action$.pipe(
    ofType(AGREGAR_EVALUACION),
    toParams,
    addAuth(),
    makeRequest(postEvaluacion),
    map(AgregarEvaluacion)
  );

const actualizarEvaluaciones = (action$) =>
  action$.pipe(
    ofType(MODIFICAR_EVALUACION),
    toParams,
    addAuth(),
    makeRequest(putEvaluacion),
    map(ModificarEvaluacion)
  );

const onGuardar = (action$) =>
  action$.pipe(
    ofType(AGREGAR_EVALUACION, MODIFICAR_EVALUACION),
    filter((action) => Resource.isData(action.payload)),
    map(
      () =>
        MostrarMensaje({
          open: true,
          mensaje: 'Evaluacion guardada con éxito',
          tipo: 'success'
        }),
      LimpiarEvaluacion()
    )
  );
const onGuardarError = (action$) =>
  action$.pipe(
    ofType(AGREGAR_EVALUACION, MODIFICAR_EVALUACION),
    filter((action) => Resource.isError(action.payload)),
    map(() =>
      MostrarMensaje({
        open: true,
        mensaje: 'Error al guardar la evaluación',
        tipo: 'error'
      })
    )
  );

const borrarEvaluacion = (action$) =>
  action$.pipe(
    ofType(ELIMINAR_EVALUACION),
    toParams,
    addAuth(),
    makeRequest(deleteEvaluacion),
    map(EliminarEvaluacion)
  );

const onEliminar = (action$) =>
  action$.pipe(
    ofType(ELIMINAR_EVALUACION),
    filter((action) => Resource.isData(action.payload)),
    mergeMap(() => [
      Evaluaciones(Resource.Query({})),
      MostrarMensaje({
        open: true,
        mensaje: 'Eliminado con éxito',
        tipo: 'success'
      })
    ])
  );

const onEliminarError = (action$) =>
  action$.pipe(
    ofType(ELIMINAR_EVALUACION),
    filter((action) => Resource.isError(action.payload)),
    map(() =>
      MostrarMensaje({
        open: true,
        mensaje: 'Error al eliminar',
        tipo: 'error'
      })
    )
  );
export default combineEpics(
  solicitarEvaluaciones,
  guardarEvaluaciones,
  onGuardarError,
  borrarEvaluacion,
  onEliminar,
  onEliminarError,
  onGuardar,
  actualizarEvaluaciones
  // limpiarFormulario
);
