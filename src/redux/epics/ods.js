import { ODS } from '../actionTypes';
import { obtenerODS } from '../api';
import { Ods } from '../actions';
import { toParams, makeRequest } from 'ghost-stories/dist/streams/resource';
import { map } from 'rxjs/operators';
import { ofType, combineEpics } from 'redux-observable';
import { addAuth } from '../api/streamHelpers';

const solicitarODS = (action$) =>
  action$.pipe(
    ofType(ODS),
    toParams,
    addAuth(),
    makeRequest(obtenerODS),
    map(Ods)
  );

export default combineEpics(solicitarODS);
