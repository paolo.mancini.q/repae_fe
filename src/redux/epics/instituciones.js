import {
  AGREGAR_INSTITUCION,
  ELIMINAR_INSTITUCION,
  INSTITUCIONES,
  MODIFICAR_INSTITUCION
} from '../actionTypes';
import {
  obtenerInstituciones,
  postInstitucion,
  deleteInstitucion,
  putInstitucion
} from '../api';
import {
  Instituciones,
  AgregarInstitucion,
  MostrarMensaje,
  EliminarInstitucion,
  LimpiarInstitucion,
  ModificarInstitucion
} from '../actions';
import { toParams, makeRequest } from 'ghost-stories/dist/streams/resource';
import { map, filter, mergeMap } from 'rxjs/operators';
import { ofType, combineEpics } from 'redux-observable';
import { Resource } from 'ghost-stories/dist/unions/Resource';
import { addAuth } from '../api/streamHelpers';

const solicitarInstituciones = (action$) =>
  action$.pipe(
    ofType(INSTITUCIONES),
    toParams,
    addAuth(),
    makeRequest(obtenerInstituciones),
    map(Instituciones)
  );
const guardarInstitucion = (action$) =>
  action$.pipe(
    ofType(AGREGAR_INSTITUCION),
    toParams,
    addAuth(),
    makeRequest(postInstitucion),
    map(AgregarInstitucion)
  );

const actualizarInstitucion = (action$) =>
  action$.pipe(
    ofType(MODIFICAR_INSTITUCION),
    toParams,
    addAuth(),
    makeRequest(putInstitucion),
    map(ModificarInstitucion)
  );

const onGuardar = (action$) =>
  action$.pipe(
    ofType(AGREGAR_INSTITUCION),
    filter((action) => Resource.isData(action.payload)),
    mergeMap(() => [
      Instituciones(Resource.Query({})),
      MostrarMensaje({
        open: true,
        mensaje: 'Institución guardada con éxito',
        tipo: 'success'
      }),
      LimpiarInstitucion()
    ])
  );
const onGuardarError = (action$) =>
  action$.pipe(
    ofType(AGREGAR_INSTITUCION),
    filter((action) => Resource.isError(action.payload)),
    map(() =>
      MostrarMensaje({
        open: true,
        mensaje: 'Error al guardar la institución',
        tipo: 'error'
      })
    )
  );

const onGuardarEditado = (action$) =>
  action$.pipe(
    ofType(MODIFICAR_INSTITUCION),
    filter((action) => Resource.isData(action.payload)),
    mergeMap(() => [
      Instituciones(Resource.Query({})),
      MostrarMensaje({
        open: true,
        mensaje: 'Institución editada con éxito',
        tipo: 'success'
      }),
      LimpiarInstitucion()
    ])
  );

const onGuardarEditadoError = (action$) =>
  action$.pipe(
    ofType(MODIFICAR_INSTITUCION),
    filter((action) => Resource.isError(action.payload)),
    mergeMap(() => [
      Instituciones(Resource.Query({})),
      MostrarMensaje({
        open: true,
        mensaje: 'No se pudo editar la institución',
        tipo: 'error'
      }),
      LimpiarInstitucion()
    ])
  );

const borrarInstitucion = (action$) =>
  action$.pipe(
    ofType(ELIMINAR_INSTITUCION),
    toParams,
    addAuth(),
    makeRequest(deleteInstitucion),
    map(EliminarInstitucion)
  );

const onEliminar = (action$) =>
  action$.pipe(
    ofType(ELIMINAR_INSTITUCION),
    filter((action) => Resource.isData(action.payload)),
    mergeMap(() => [
      Instituciones(Resource.Query({})),
      MostrarMensaje({
        open: true,
        mensaje: 'Institución eliminada con éxito',
        tipo: 'success'
      })
    ])
  );

const onEliminarError = (action$) =>
  action$.pipe(
    ofType(ELIMINAR_INSTITUCION),
    filter((action) => Resource.isError(action.payload)),
    map(() =>
      MostrarMensaje({
        open: true,
        mensaje: 'Error al eliminar la institución',
        tipo: 'error'
      })
    )
  );

export default combineEpics(
  solicitarInstituciones,
  guardarInstitucion,
  onGuardar,
  borrarInstitucion,
  onEliminar,
  onEliminarError,
  actualizarInstitucion,
  onGuardarError,
  onGuardarEditado,
  onGuardarEditadoError
);
