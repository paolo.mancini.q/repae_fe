import {
  RANGO_ETARIO,
  REGION,
  SEXO,
  TIPO_INSTITUCION,
  TIPO_LINEAMIENTO
} from '../actionTypes';
import {
  obtenerSexo,
  obtenerTipoInstitucion,
  obtenerTipoLineamiento,
  obtenerRegion,
  obtenerRangoEtario
} from '../api';
import {
  RangoEtario,
  Region,
  Sexo,
  TipoInstitucion,
  TipoLineamiento
} from '../actions';
import { toParams, makeRequest } from 'ghost-stories/dist/streams/resource';
import { map } from 'rxjs/operators';
import { ofType, combineEpics } from 'redux-observable';
import { addAuth } from '../api/streamHelpers';

const solicitarSexo = (action$) =>
  action$.pipe(
    ofType(SEXO),
    toParams,
    addAuth(),
    makeRequest(obtenerSexo),
    map((res) => res.map(({ data }) => data)),
    map(Sexo)
  );

const solicitarRegion = (action$) =>
  action$.pipe(
    ofType(REGION),
    toParams,
    addAuth(),
    makeRequest(obtenerRegion),
    map((res) => res.map(({ data }) => data)),
    map(Region)
  );

const solicitarRangoEtario = (action$) =>
  action$.pipe(
    ofType(RANGO_ETARIO),
    toParams,
    addAuth(),
    makeRequest(obtenerRangoEtario),
    map((res) => res.map(({ data }) => data)),
    map(RangoEtario)
  );

const solicitarTipoInstitucion = (action$) =>
  action$.pipe(
    ofType(TIPO_INSTITUCION),
    toParams,
    addAuth(),
    makeRequest(obtenerTipoInstitucion),
    map((res) => res.map(({ data }) => data)),
    map(TipoInstitucion)
  );

const solicitarTipoLineamiento = (action$) =>
  action$.pipe(
    ofType(TIPO_LINEAMIENTO),
    toParams,
    addAuth(),
    makeRequest(obtenerTipoLineamiento),
    map((res) => res.map(({ data }) => data)),
    map(TipoLineamiento)
  );

export default combineEpics(
  solicitarSexo,
  solicitarTipoInstitucion,
  solicitarTipoLineamiento,
  solicitarRegion,
  solicitarRangoEtario
);
