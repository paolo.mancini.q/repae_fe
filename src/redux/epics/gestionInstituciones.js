import { GESTION_INSTITUCIONES } from '../actionTypes';
import { obtenerInstituciones } from '../api';
import { GestionInstituciones } from '../actions';
import { toParams, makeRequest } from 'ghost-stories/dist/streams/resource';
import { map } from 'rxjs/operators';
import { ofType, combineEpics } from 'redux-observable';
import { addAuth } from '../api/streamHelpers';

const solicitarGestionInstituciones = (action$) =>
  action$.pipe(
    ofType(GESTION_INSTITUCIONES),
    toParams,
    addAuth(),
    makeRequest(obtenerInstituciones),
    map(GestionInstituciones)
  );

export default combineEpics(solicitarGestionInstituciones);
