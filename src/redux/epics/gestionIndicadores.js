import { GESTION_INDICADORES } from '../actionTypes';
import { obtenerIndicadores } from '../api';
import { GestionIndicadores } from '../actions';
import { toParams, makeRequest } from 'ghost-stories/dist/streams/resource';
import { map } from 'rxjs/operators';
import { ofType, combineEpics } from 'redux-observable';
import { addAuth } from '../api/streamHelpers';

const solicitarGestionIndicadores = (action$) =>
  action$.pipe(
    ofType(GESTION_INDICADORES),
    toParams,
    addAuth(),
    makeRequest(obtenerIndicadores),
    map(GestionIndicadores)
  );

export default combineEpics(solicitarGestionIndicadores);
