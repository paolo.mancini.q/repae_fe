export interface SignUpFormProps {
  onGoogleSignUp: () => void;
  onFacebookSignup: () => void;
  onTwitterSignup: () => void;
}
