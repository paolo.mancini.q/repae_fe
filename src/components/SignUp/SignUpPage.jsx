import React from 'react';
import SignUpForm from './SignUpForm';

export default (props) => {
  return (
    <div>
      <h1>SignUp</h1>
      <SignUpForm {...props} />
    </div>
  );
};
