import React from 'react';
import LogInForm from './LogInForm';

export default () => {
  return (
    <div>
      <LogInForm />
    </div>
  );
};
