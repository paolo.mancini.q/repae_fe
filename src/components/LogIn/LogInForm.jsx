import React from 'react';
import { Grid } from '@material-ui/core';
import hero9 from './hero-9.jpg';
import Tabs from './Tabs';

export default () => {
  return (
    <div className="app-wrapper min-vh-100 bg-white">
      <div className="app-main min-vh-100">
        <div className="app-content p-0">
          <div className="app-inner-content-layout--main">
            <div className="flex-grow-1 w-100 d-flex align-items-center">
              <div className="bg-composed-wrapper--content">
                <Grid container spacing={0} className="min-vh-100">
                  <Grid
                    item
                    xs={12}
                    md={4}
                    lg={5}
                    className="d-flex align-items-center">
                    <div className="hero-wrapper w-100 bg-composed-wrapper bg-plum-plate min-vh-100">
                      <div className="flex-grow-1 w-100 d-flex align-items-center">
                        <div
                          className="bg-composed-wrapper--image"
                          style={{ backgroundImage: 'url(' + hero9 + ')' }}
                        />
                        <div className="bg-composed-wrapper--bg bg-premium-dark opacity-5" />
                        <div className="bg-composed-wrapper--content p-5"></div>
                      </div>
                    </div>
                  </Grid>
                  <Tabs />
                </Grid>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
