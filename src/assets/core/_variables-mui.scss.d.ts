export interface I_variablesMuiScss {
  primary: stringi;
  primaryLight: string;
  primaryDark: string;
  secondary: string;
  inheritDefault1: string;
  inheritDefault2: string;
  second: string;
  indigo: string;
  purple: string;
  pink: string;
  red: string;
  orange: string;
  yellow: string;
  green: string;
  teal: string;
  cyan: string;
}

export const styles: _variablesMuiScss;
export default styles;
