import firebase from 'firebase';
import app from 'firebase/app';
import 'firebase/auth';
const config = {
  apiKey: process.env.REACT_APP_FIREBASE_API_KEY,
  authDomain: process.env.REACT_APP_FIREBASE_AUTH_DOMAIN,
  databaseURL: process.env.REACT_APP_FIREBASE_DATABASE_URL,
  projectId: process.env.REACT_APP_FIREBASE_PROJECT_ID,
  storageBucket: process.env.REACT_APP_FIREBASE_STORAGE_BUCKET,
  messagingSenderId: process.env.REACT_APP_FIREBASE_MESSAGING_SENDER_ID
};
class Firebase {
  constructor() {
    // *** Auth API ***
    this.doSignOut = () => this.auth.signOut();
    this.doGoogleRegister = () =>
      this.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider());
    this.doFacebookRegister = () =>
      this.auth.signInWithPopup(new firebase.auth.FacebookAuthProvider());
    this.doTwitterRegister = () =>
      this.auth.signInWithPopup(new firebase.auth.TwitterAuthProvider());
    this.getIdToken = () => {
      var _a;
      return (_a = this.auth.currentUser) === null || _a === void 0
        ? void 0
        : _a.getIdToken(true);
    };
    app.initializeApp(config);
    this.auth = app.auth();
  }
}
export default Firebase;

export const instance = new Firebase();
